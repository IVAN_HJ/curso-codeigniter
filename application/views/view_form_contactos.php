<h1>Nuevo Contacto</h1>

<?php
$input_con_email = array(
    'name' => 'con_email',
    'id' => 'con_email',
    'maxlength' => '120',
    'size' => '100',
);

$input_con_nombre = array(
    'name' => 'con_nombre',
    'id' => 'con_nombre',
    'maxlength' => '60',
    'size' => '50',
);

$input_con_telefono = array(
    'name' => 'con_telefono',
    'id' => 'con_telefono',
    'maxlength' => '20',
    'size' => '18',
);
$input_con_edad = array(
    'name' => 'con_edad',
    'id' => 'con_edad',
    'maxlength' => '3',
    'size' => '4',
);

$opciones = array(
    '0' => 'Inactivo',
    '1' => 'Activo'
);
?>
<br><br>
<?php echo validation_errors();?>
<?php echo form_open() ?><br>

<?php echo form_label('Email') ?><br>
<?php echo form_input($input_con_email) ?><br><br>

<?php echo form_label('Nombre') ?><br>
<?php echo form_input($input_con_nombre) ?><br><br>

<?php echo form_label('Telefono') ?><br>
<?php echo form_input($input_con_telefono) ?><br><br>

<?php echo form_label('Edad') ?><br>
<?php echo form_input($input_con_edad) ?><br><br>

<?php echo form_label('Estatus') ?><br>
<?php echo form_dropdown('con_estatus',$opciones) ?><br><br>
<?php echo form_submit('btn_enviar','Guardar')?>

<?php echo form_close() ?>
<!--<form method="post" action="" accept-charset="utf8">
    <label for="">Nombre</label>
    <input type="text" name="nombre"/>
</form>-->