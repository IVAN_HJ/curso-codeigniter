<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contactos extends CI_Controller {

    public function index() {
//            $data['nombre']= 'Ivan';
//                echo 'Hola mundo 2!';
        $this->load->model('M_contactos');
        $data['listado'] = $this->M_contactos->get_todos();

        $this->load->view('view_list_contactos', $data);
    }

    public function agregar() {

        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->input->post()) {
            $this->form_validation->set_rules('con_email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('con_nombre', 'Nombre', 'required|min_length[3]');
            $this->form_validation->set_rules('con_edad', 'Edad', 'required|integer');
            $this->form_validation->set_rules('con_email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == true) {
                echo 'informacion recibida';
                print_r($this->input->post());
            } else {
                $this->load->view('view_form_contactos');
            }
        } else {
            $this->load->view('view_form_contactos');
        }
    }

}

//fin del archivo contactos.php